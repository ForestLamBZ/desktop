# Desktop

## Description
A personalised desktop configuration script for a minimal gnome, gdm3 debian 12/ubuntu 22.04 x86 system

## Usage
su -  
apt install -y wget  
bash -c "$(wget -O - "https://gitlab.com/ForestLamBZ/desktop/-/raw/main/debian12.sh")"  
/  
bash -c "$(wget -O - "https://gitlab.com/ForestLamBZ/desktop/-/raw/main/ubuntu2204.sh")"  
