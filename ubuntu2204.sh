#!/bin/bash

#check root
if [ "$EUID" != 0 ]; then
  echo "Root yourself"
  exit
fi

#user prompt preference
echo -e "user preference prompt [y/n]"
read preference
if [ $preference == n ]; then
  echo "ok"
elif [ $preference != y ]; then
  echo "not ok"
fi

#redirecting apt to nala in bashrc
apt update
apt install -y nala
user1=$(awk -F: '$3 > 0 && $7 ~ /(bash|zsh)$/ {print $1}' /etc/passwd)
echo 'apt() {
  if [ "$1" = "--apt" ]; then
    shift
    command apt "$@"
  else
    command nala "$@"
  fi
}
sudo() {
  if [ "$1" == "apt" ] && [ "$2" == "--apt" ]; then
    shift
    shift
    command sudo apt "$@"
  elif [ "$1" = "apt" ]; then
    shift
    command sudo nala "$@"
  else
    command sudo "$@"
  fi
}' | tee -a /home/$user1/.bashrc /root/.bashrc
source ~/.bashrc
apt --version

#add user to sudoer group to perform user installation
echo -r "$user1 ALL=(ALL:ALL) NOPASSWD:ALL" | sudo EDITOR='tee -a' visudo

#main debian package installation
apt fetch --non-free --ubuntu jammy --auto -y --fetches 5
sed -i '/main/ {/#/! s/$/ non-free-firmware/}' /etc/apt/sources.list
apt update
apt install -y nvidia-driver firmware-misc-nonfree \
gnome-session gnome-shell gnome-applets gnome-control-center network-manager gnome-system-monitor isc-dhcp-client desktop-base avahi-daemon \
gnome-bluetooth gnome-bluetooth-sendto gnome-remote-desktop gdm3 curl lshw inxi fonts-noto-cjk fonts-noto-cjk-extra gnome-terminal gnome-themes-extra-data \
chromium feh ffmpeg gimp git libreoffice vlc xdg-utils yt-dlp libfuse2 neofetch copyq nautilus lshw lynx

#independent packages
wget https://mega.nz/linux/repo/xUbuntu_23.04/amd64/megasync-xUbuntu_23.04_amd64.deb
apt install -y "$PWD/megasync-xUbuntu_23.04_amd64.deb"

wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor > signal-desktop-keyring.gpg
cat signal-desktop-keyring.gpg | sudo tee /usr/share/keyrings/signal-desktop-keyring.gpg > /dev/null
echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] https://updates.signal.org/desktop/apt xenial main' |\
  tee /etc/apt/sources.list.d/signal-xenial.list
apt update
apt install -y signal-desktop 

mkdir -p /home/$user1/app/build
cd /home/$user1/app/build
curl "https://ftp.mozilla.org/pub/firefox/releases/118.0/linux-x86_64/en-US/firefox-118.0.tar.bz2" -o firefox.tar.bz2
tar -xjvf firefox.tar.bz2
ln -s ./firefox/firefox /usr/bin/firefox
chmod 770 /usr/bin/firefox
mkdir 
touch /usr/share/applications/firefox.desktop
echo -e "[Desktop Entry]
Name=Firefox
Exec=/home/$user1/app/build/firefox/firefox
Icon=/home/$user1/app/build/firefox/browser/chrome/icons/default/default128.png" >> /usr/share/applications/firefox.desktop

rm firefox.tar.bz2
rm ./megasync-xUbuntu_23.04_amd64.deb
rm ./signal-desktop-keyring.gpg
apt purge -y yelp

#closing
apt -y upgrade
neofetch
read -p "Restart[]"
systemctl reboot
